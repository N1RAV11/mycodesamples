GOL gol;

void setup() {
  size(420, 595);  
  gol = new GOL();
}

void draw() {
  background(255);

  gol.generate();
  delay(100);
  gol.display();
  saveFrame();
}

void mousePressed() {
  gol.init();
}
