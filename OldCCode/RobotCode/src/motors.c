#include "motors.h"

// sets up timer 1 & 2
void setupMotorTimer(void) {
  // enable TIM2 clock for PWM output
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  // set TIM2 ARR to resolution (frequency of PWM)
  TIM2->ARR = 0xF9F; //2 kHz (opto-coupler takes 3us to switch, 10.6% of 1 pulse)
  // clear TIM2 channel 1,2 mode
  TIM2->CCMR1 &= ~(TIM_CCMR1_OC1M | TIM_CCMR1_OC2M);
  // clear TIM2 channel 3,4 mode
  TIM2->CCMR2 &= ~(TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
  // set TIM2 channel 1,2 to PWM mode 2
  TIM2->CCMR1 |= (TIM_CCMR1_OC1M | TIM_CCMR1_OC2M);
  // set TIM2 channel 3,4 to PWM mode 2
  TIM2->CCMR2 |= (TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
  // enable TIM2 channel 1,2 output
  TIM2->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);
  // enable TIM2 channel 3,4 output
  TIM2->CCER |= (TIM_CCER_CC3E | TIM_CCER_CC4E);

  // FOR THE ARM MOTOR------------------------------
  TIM2->CCMR1 &= ~TIM_CCMR1_OC1M;
  TIM2->CCMR1 |= (TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1);

  // enable TIM2 counter
  TIM2->CR1 |= 0x01;

//  // enable TIM1 clock for PWM output
//  RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
//  // set TIM1 ARR to resolution (frequency of PWM)
//  TIM1->ARR = 0xF9F; //2 kHz (opto-coupler takes 3us to switch, 10.6% of 1 pulse)
//  // clear TIM1 channel 1,2 mode
//  TIM1->CCMR1 &= ~(TIM_CCMR1_OC1M | TIM_CCMR1_OC2M);
//  // clear TIM1 channel 3,4 mode
//  TIM1->CCMR2 &= ~(TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
//  // set TIM1 channel 1,2 to PWM mode 2
//  TIM1->CCMR1 |= (TIM_CCMR1_OC1M | TIM_CCMR1_OC2M);
//  // set TIM1 channel 3,4 to PWM mode 2
//  TIM1->CCMR2 |= (TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
//  // enable TIM1 channel 1,2 output
//  TIM1->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);
//  // enable TIM1 channel 3,4 output
//  TIM1->CCER |= (TIM_CCER_CC3E | TIM_CCER_CC4E);
//  // enable TIM1 counter
//  TIM1->CR1 |= 0x01;

}

void initMotor(Motor *motor) {
  if ( (motor->fwd_reg == GPIOA) || (motor->rev_reg == GPIOA) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
  } if ( (motor->fwd_reg == GPIOB) || (motor->rev_reg == GPIOB) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  } if ( (motor->fwd_reg == GPIOC) || (motor->rev_reg == GPIOC) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
  } if ( (motor->fwd_reg == GPIOD) || (motor->rev_reg == GPIOD) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIODEN;
  } if ( (motor->fwd_reg == GPIOE) || (motor->rev_reg == GPIOE) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
  } if ( (motor->fwd_reg == GPIOF) || (motor->rev_reg == GPIOF) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
  }

  if (motor->fwd_reg != 0) {
    motor->fwd_reg->MODER &= ~(0x3 << (motor->fwd_pin * 2));
    motor->fwd_reg->MODER |= (0x1 << (motor->fwd_pin * 2));
    motor->rev_reg->MODER &= ~(0x3 << (motor->rev_pin * 2));
    motor->rev_reg->MODER |= (0x1 << (motor->rev_pin * 2));
  }

}

//void initPWM_B10_11(void) {
//    //enable GPIOB
//    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
//    // enable TIM2 clock for PWM output
//    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
//    // clear GPIOB 10 mode
//    GPIOB->MODER &= ~(GPIO_MODER_MODER10 | GPIO_MODER_MODER11);
//    // set GPIOB 10 to alternative function mode
//    GPIOB->MODER |= (GPIO_MODER_MODER10_1 | GPIO_MODER_MODER11_1);
//    // clear GPIOB 10 & 11 alternative function
//    GPIOB->AFR[1] &= ~0xFF00;
//    // set GPIOB 10 & 11 alternative function to AF2 (TIM2_CH3)
//    GPIOB->AFR[1] |= 0x2200;
//
//    // set TIM2 ARR to resolution (frequency of PWM)
//    TIM2->ARR = 0xF9F; //2 kHz (opto-coupler takes 3us to switch, 10.6% of 1 pulse)
//    // clear TIM2 channel 3,4 mode
//    TIM2->CCMR2 &= ~(TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
//    // set TIM2 channel 3,4 to PWM mode 2222222222222
////    TIM2->CCMR2 |= 0x6060;
//    TIM2->CCMR2 |= (TIM_CCMR2_OC3M | TIM_CCMR2_OC4M);
//    // enable TIM2 channel 3,4 output
//    TIM2->CCER |= (TIM_CCER_CC3E | TIM_CCER_CC4E);
//    // enable TIM2 counter
//    TIM2->CR1 |= 0x01;
//}

void setMotor(Motor *motor, uint8_t mode, uint16_t speed) {
  if (mode == 0) {      // coast
    (motor->fwd_reg)->ODR &= ~(0x01 << motor->fwd_pin);
    (motor->rev_reg)->ODR &= ~(0x01 << motor->rev_pin);
  } else if (mode == 1) { // fwd_pin
	  (motor->fwd_reg)->ODR |= (0x01 << motor->fwd_pin);
    (motor->rev_reg)->ODR &= ~(0x01 << motor->rev_pin);
	} else if (mode == 2) { // rev_pin
	  (motor->fwd_reg)->ODR &= ~(0x01 << motor->fwd_pin);
    (motor->rev_reg)->ODR |= (0x01 << motor->rev_pin);
	} else if (mode == 3) { // brake
    (motor->fwd_reg)->ODR |= (0x01 << motor->fwd_pin);
    (motor->rev_reg)->ODR |= (0x01 << motor->rev_pin);
	}

	motor->set_speed = speed;
	if (speed > motor->current_speed) motor->ramping = true;
	else {
	  motor->ramping = false;
	  motor->current_speed = speed;
	  *motor->tim_reg = speed;
	}
	motor->state = mode;
}
