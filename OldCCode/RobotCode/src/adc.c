#include "adc.h"

void initADC(uint8_t resolution) {
  RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;

  ADC1->CR |= ADC_CR_ADCAL;
  while ((ADC1->CR & ADC_CR_ADCAL) != 0)
    ;
  ADC1->CR |= ADC_CR_ADEN;
  while ((ADC1->ISR & ADC_ISR_ADRDY) == 0)
    ;

  // probably always want continuous mode (unless reading 2 channels simultaneously!)
//	ADC1->CFGR1 |= (ADC_CFGR1_CONT | ADC_CFGR1_OVRMOD);

  setADCResolution(resolution);
}

void setADCResolution(uint8_t resolution) {
  ADC1->CFGR1 &= ~ADC_CFGR1_RES;
  ADC1->CFGR1 |= (resolution << 3);
}

uint16_t readADCValue() {
  ADC1->CR |= ADC_CR_ADSTART;
  while ( (ADC1->ISR & ADC_ISR_EOC) == 0 );
  ADC1->ISR &= ~ADC_ISR_EOC;
  return ADC1->DR;
}
