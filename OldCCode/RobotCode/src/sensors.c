#include "sensors.h"

void initLineSensor(LineSensor * sensor) {
  if ( (sensor->left_reg == GPIOA) || (sensor->right_reg == GPIOA) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
  } if ( (sensor->left_reg == GPIOB) || (sensor->right_reg == GPIOB) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  } if ( (sensor->left_reg == GPIOC) || (sensor->right_reg == GPIOC) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
  } if ( (sensor->left_reg == GPIOD) || (sensor->right_reg == GPIOD) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIODEN;
  } if ( (sensor->left_reg == GPIOE) || (sensor->right_reg == GPIOE) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
  } if ( (sensor->left_reg == GPIOF) || (sensor->right_reg == GPIOF) ) {
    RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
  }

  sensor->left_reg->MODER |= (0x3 << (sensor->left_pin * 2));
  sensor->right_reg->MODER |= (0x3 << (sensor->right_pin * 2));

  RCC->AHBENR |= RCC_AHBENR_DMA1EN;
  DMA1_Channel1->CCR |= (DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_MINC | DMA_CCR_CIRC);
  DMA1_Channel1->CNDTR = 4; // 4 bytes (2 16-bit values)
  DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
}

void initDistanceSensor(void) {
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
  GPIOA->MODER &= ~GPIO_MODER_MODER7;
  GPIOA->MODER |= GPIO_MODER_MODER7_1;
  GPIOA->AFR[0] &= ~GPIO_AFRL_AFR7;
  // set PA7 to AF1 (TIM3 CH1)
  GPIOA->AFR[0] |= (0x01 << 28);
  initTimer(TIM17,15 ,2798);
  TIM17->CR1 |= TIM_CR1_OPM;
  initTimer(TIM3, 0x4AF, 0);
  TIM3->CCMR1 &= ~TIM_CCMR1_OC2M;
  TIM3->CCMR1 |= (TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M);
  TIM3->CCER |= TIM_CCER_CC2E;
  TIM3->CCR2 = 0x258;
  TIM3->DIER |= TIM_DIER_CC2IE;
  NVIC_EnableIRQ(TIM3_IRQn);
//  startTimer(TIM3);

  // set up DAC

//  // set PA4 (DAC1 output) to analog mode
  GPIOA->MODER |= GPIO_MODER_MODER4;

  RCC->APB1ENR |= RCC_APB1ENR_DACEN;

  DAC->CR |= DAC_CR_EN1;

  DAC->DHR12R1 = 1500; // 1.2V

  // set up comparator
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN;
  //set PA1 to analog mode
  GPIOA->MODER |= GPIO_MODER_MODER1;

  // DAC1 output for inverting input
  COMP->CSR |= COMP_CSR_COMP1INSEL_2;

  // enable EXTI interrupt for COMP1
  EXTI->IMR |= EXTI_IMR_MR21;
  // enable rising edge trigger
  EXTI->RTSR |= EXTI_RTSR_TR21;
}

void initBeamSensor(void) {
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN;
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI7_PB;
  EXTI->FTSR |= EXTI_FTSR_TR7;
}

void startBeamSensing(void) {
  NVIC_EnableIRQ(EXTI4_15_IRQn);
  EXTI->IMR |= EXTI_IMR_MR7;
}

void stopBeamSensing(void) {
  EXTI->IMR &= ~EXTI_IMR_MR7;
}

void startDistanceSensing(void) {
  NVIC_EnableIRQ(TIM3_IRQn);
  EXTI->IMR |= EXTI_IMR_MR21;
  startTimer(TIM3);
}

void stopDistanceSensing(void) {
  NVIC_DisableIRQ(TIM3_IRQn);
  EXTI->IMR &= ~EXTI_IMR_MR21;
  stopTimer(TIM3);
}

void startLineReading(LineSensor * sensor) {
  ADC1->CFGR1 |= ADC_CFGR1_CONT;

  DMA1_Channel1->CMAR = (uint32_t)&sensor->left;
  DMA1_Channel1->CCR |= DMA_CCR_EN;

  ADC1->CFGR1 |= ADC_CFGR1_DMAEN;
  ADC1->CFGR1 |= ADC_CFGR1_DMACFG;


  ADC1->CHSELR = ( (uint16_t)(0x01 << sensor->left_pin) | (uint16_t)(0x01 << sensor->right_pin) );

  ADC1->IER |= ADC_IER_EOSEQIE;
  NVIC_EnableIRQ(ADC1_COMP_IRQn);
  ADC1->CR |= ADC_CR_ADSTART;
}

void pauseLineReading() {
  ADC1->IER &= ~ADC_IER_EOSEQIE;
}

void resumeLineReading() {
  ADC1->IER |= ADC_IER_EOSEQIE;
}
