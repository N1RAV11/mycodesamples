#include "timers.h"

void initTimer(TIM_TypeDef * timer, uint32_t ARR, uint32_t PSC) {
	if (timer == TIM6) {
		RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	} else if (timer == TIM17) {
	  RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
	} else if (timer == TIM3) {
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
  } else if (timer == TIM2) {
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  }
	setTimerParameters(timer, ARR, PSC);
	timer->EGR |= TIM_EGR_UG;
	timer->SR &= ~TIM_SR_UIF;
}

void setTimerParameters(TIM_TypeDef * timer, uint32_t ARR, uint32_t PSC) {
	timer->ARR = ARR;
	timer->PSC = PSC;
}

void startTimer(TIM_TypeDef * timer) {
	timer->CR1 |= TIM_CR1_CEN;
}

void stopTimer(TIM_TypeDef * timer) {
	timer->CR1 &= ~TIM_CR1_CEN;
}

void resetTimer(TIM_TypeDef * timer) {
	timer->CNT = 0;
}
