//#include "main.h"
//
//void main() {
//  RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;
//  GPIOB->MODER |= 0x5555;
//  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN;
//  EXTI->IMR |= EXTI_IMR_MR0;
//  EXTI->FTSR |= EXTI_FTSR_TR0;
//  NVIC_EnableIRQ(EXTI0_1_IRQn);
//}
//
//void EXTI0_1_IRQHandler() {
//  GPIOB->ODR = 0xFF;
//}
//

#include "main.h"

uint32_t time;

uint8_t line_count = 0;
bool direction = 1; // 1 = forward, 2 = backward
bool sensed_baton = false;
bool on_black = false;

uint8_t left = 0;
uint8_t right = 0;

//uint16_t max_speed = 0xCA0;
uint16_t max_speed = 0xAA0;

uint16_t config_values[6]; // f_b, f_w, b_b, b_w, c_r, c_g

static configMode mode = startup;

Motor motorRight = { .tim_reg = &TIM2->CCR3, .rev_pin = 13, .fwd_pin = 14,
    .rev_reg = GPIOC, .fwd_reg = GPIOC };

Motor motorLeft = { .tim_reg = &TIM2->CCR4, .rev_pin = 2, .fwd_pin = 1,
    .rev_reg = GPIOB, .fwd_reg = GPIOB };

Motor motorClaw = { .tim_reg = &TIM2->CCR2, .rev_pin = 4, .fwd_pin = 5,
    .rev_reg = GPIOB, .fwd_reg = GPIOB };

// 0 because we don't actually have any control pins
Motor motorArm = { .tim_reg = &TIM2->CCR1, .rev_pin = 4, .fwd_pin = 5,
    .rev_reg = 0, .fwd_reg = 0 };

Motor *motors[] = { &motorLeft, &motorRight, &motorClaw, &motorArm };

LineSensor frontLine = { .left_pin = 6, .right_pin = 5, .left_reg = GPIOA,
    .right_reg = GPIOA };

LineSensor backLine = { .left_pin = 6, .right_pin = 5, .left_reg = GPIOA,
    .right_reg = GPIOA };

LineSensor backLine;

void main(void) {

  setClockto48();

  // left motor
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  GPIOB->MODER |= GPIO_MODER_MODER10_1;
  GPIOB->AFR[1] |= 0x200;

  // right motor
  GPIOB->MODER |= GPIO_MODER_MODER11_1;
  GPIOB->AFR[1] |= 0x2000;

  // claw motor
  GPIOB->MODER |= GPIO_MODER_MODER3_1;
  GPIOB->AFR[0] |= 0x2000;

  // arm motor
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
  GPIOA->MODER |= GPIO_MODER_MODER15_1;
  GPIOA->AFR[1] |= 0x20000000;

//  setupMotorTimer();
//
//  TIM1->CCR1 |= max_speed;
//  volatile int x = GPIOA->OTYPER;
//  while (1) {
//    volatile int y = GPIOA->OTYPER;
//  }

//  GPIOB->MODER |= GPIO_MODER_MODER9_0;
//  GPIOB->ODR |= (0x01 << 9);
//  delay2(250000);
//  GPIOB->ODR = 0;
//  while(1);

  initMotor(&motorLeft);
  initMotor(&motorRight);
  initMotor(&motorClaw);
  initMotor(&motorArm);

  setup = true;

  NVIC_SetPriority(TIM3_IRQn, 25);
  NVIC_SetPriority(TIM16_IRQn, 20);
  NVIC_SetPriority(ADC1_COMP_IRQn, 15);

  initLineSensor(&frontLine);
  initLineSensor(&backLine);
  initDistanceSensor();
  initBeamSensor();

  robotSetup(config_values);
  setup = false;
//	mode = start;
  GPIOA->MODER |= GPIO_MODER_MODER0;
//  startBeamSensing();
//  while(1);
//  waitForGreen(config_values);
  setMotor(&motorLeft, 1, max_speed);
  setMotor(&motorRight, 1, max_speed);
//  setMotor(&motorClaw, 1, max_speed);

  setTimerParameters(TIM6, 9, 4799);
  startTimer(TIM6);
  NVIC_EnableIRQ(TIM6_DAC_IRQn);
  mode = forward;
  lcd_command(CLEAR);
  lcd_putstring("FORWARD");
  startLineReading(&frontLine);
//  NVIC_EnableIRQ(ADC1_COMP_IRQn);
//  startDistanceSensing();

  while (mode != look_for_baton) {
    int x = 0;
  }
  setMotor(&motorLeft, 0, 0xFA0);
  setMotor(&motorRight, 0, 0xFA0);
  while(1);
}

void TIM6_DAC_IRQHandler(void) {

  if (setup) {
    if (calibration == 0) {
      NVIC_DisableIRQ(EXTI4_15_IRQn);
      NVIC_DisableIRQ(TIM6_DAC_IRQn);
      stopTimer(TIM6);
      TIM6->SR &= ~TIM_SR_UIF;

      changeMode(config_values, &setupMode, TIMEOUT);

    } else if (calibration == 1) {
      TIM6->SR &= ~TIM_SR_UIF;
      readSensorValue(config_values, setupMode);

    }
  } else {
    TIM6->SR &= ~TIM_SR_UIF;

    // ramp any applicable motors
    for (int i = 0; i < 4; ++i) {
      if (motors[i]->ramping) {
        if (motors[i]->current_speed < motors[i]->set_speed) {
          *motors[i]->tim_reg += 10;
          motors[i]->current_speed += 10;
        } else
          motors[i]->ramping = false;
      }
    }
  }

}

void EXTI4_15_IRQHandler(void) {
  if (EXTI->PR & EXTI_PR_PR8) { // control button
    EXTI->PR |= EXTI_PR_PR8;
    if (setup) {
      NVIC_DisableIRQ(EXTI4_15_IRQn);

      if (calibration == 0) {
        NVIC_DisableIRQ(TIM6_DAC_IRQn);
        stopTimer(TIM6);
        resetTimer(TIM6);
      }

      // mask line so that any button presses during flashing sequence are ignored
      EXTI->IMR &= ~EXTI_IMR_MR8;

      changeMode(config_values, &setupMode, PRESS);
//    } else if (EXTI->PR & EXTI_PR_PR7) {
//      EXTI->PR |= EXTI_PR_PR7;
//      pauseLineReading();
//    }
  }
  } else if (EXTI->PR & EXTI_PR_PR7) {
    EXTI->PR |= EXTI_PR_PR7;
    direction = 2;
    stopBeamSensing();
//    pauseLineReading();
//    setMotor(&motorLeft, 1, 0x0);
//    setMotor(&motorRight, 1, 0x0);
//
//    setMotor(&motorClaw, 1, 0xFA0);
//    TIM2->CCR2 = 0xFA0;
//    delay2(2500000);
////    setMotor(&motorClaw, 0, 0x000);
////    TIM2->CCR2 = 0x0;
//    mode = backwards;
//    max_speed = 0xAA0;
//
//    TIM2->CCR1 = 0xFA0;
//    setMotor(&motorArm, 1, 0xFA0);
//    delay2(4000000);
//    TIM2->CCR1 = 0x0;
//    setMotor(&motorArm, 1, 0x0);
//
//    setMotor(&motorClaw, 2, 0xFA0);
//    TIM2->CCR2 = 0xFA0;
//    delay2(4000000);
//    setMotor(&motorClaw, 0, 0xFA0);
//
//    direction = 2;
//    line_count = 0;
//    startLineReading(&backLine);

  }
}

void ADC1_COMP_IRQHandler(void) {
  // check if the ADC or the comparator has caused the interrupt
  if ( (EXTI->PR & EXTI_PR_PR21) != 0) {
    time = TIM17->CNT;
    stopTimer(TIM17);


    // disable comparator
    COMP->CSR &= ~COMP_CSR_COMP1EN;

//    dist = 34300 * (time / 2);
//    dist /= 10000000;

    char test[25];
    if (time != 0) {
//      sprintf(test, "%02u", time);
      sensed_baton = true;
      mode = look_for_baton;
      stopDistanceSensing();
      startBeamSensing();
    }

    //itoa(1000, test, 10);

    lcd_command(CURSOR_HOME);
    lcd_putstring(test);

    // ACK interrupt
    EXTI->PR |= EXTI_PR_PR21;
//    resumeLineReading();
    //    GPIOB->ODR = 1;
  }
//  if ((ADC1->ISR & ADC_ISR_EOSEQ) != 0) {
  else {
    ADC1->ISR &= ~ADC_ISR_EOSEQ;
//    lcd_command(CURSOR_HOME);

    uint8_t status = 0;
//    config_values[0] = 2000;
    // black >>>>> white
    if (mode != backwards) {
      if (frontLine.left > (config_values[0] * 0.9)) {
  //      sprintf(string, "BLACK:%u   ", frontLine.left);
        if (left >= 100) {
          status |= 0x1;
        }
        else {
          left++;
//          status |= 0x2;
        }
      } else {
          left = 0;
  //      sprintf(string, "WHITE:%u   ", frontLine.left);
      }
  //    lcd_putstring(string);
      lcd_command(LINE_TWO);
      if (frontLine.right > (config_values[0] * 0.9)) {
  //      sprintf(string, "BLACK:%u   ", frontLine.right);
        if (right >= 100) {
          status |= 0x2;
        }
        else {
          right++;
        }
      } else {
        right = 0;
  //      sprintf(string, "WHITE:%u   ", frontLine.right);
      }
    } else {
      if (backLine.left > (config_values[0] * 0.9)) {
      //      sprintf(string, "BLACK:%u   ", frontLine.left);
            if (left >= 100) {
              status |= 0x1;
            }
            else {
              left++;
            }
          } else {
              left = 0;
      //      sprintf(string, "WHITE:%u   ", frontLine.left);
          }
      //    lcd_putstring(string);
          lcd_command(LINE_TWO);
          if (backLine.right > (config_values[0] * 0.9)) {
      //      sprintf(string, "BLACK:%u   ", frontLine.right);
            if (right >= 100) {
              status |= 0x2;
            }
            else {
              right++;
            }
          } else {
            right = 0;
      //      sprintf(string, "WHITE:%u   ", frontLine.right);
          }
    }
//    lcd_putstring(string);

    // 0x1 = left, 0x2 = right
//    if ( (direction == 0x0) && (status == 0x1)) {
//      direction = 0x1;
//    } else if ( (direction == 0x0) && (status == 0x2)) {
//      direction = 0x2;
//    } else if ( (direction == 0x1) && (status == 0)) {
//      status = 0x1;
//    } else if ( (direction == 0x2) && (status == 0)) {
//      status = 0x2;
//    } else if ( (direction == 0x1) && (status == 0x2)) {
//      direction = 0;
//    } else if ( (direction == 0x2) && (status == 0x1)) {
//      direction = 0;
//    }

    switch (status) {
    case 0: // no black
      setMotor(&motorLeft, direction, max_speed);
      setMotor(&motorRight, direction, max_speed);
      on_black = false;
      break;

    case 0x1: // black left
      setMotor(&motorLeft, direction, max_speed);
      setMotor(&motorRight, direction, 0);
      on_black = false;
      break;

    case 0x2: // black right
      setMotor(&motorLeft, direction, 0);
      setMotor(&motorRight, direction, max_speed);
      on_black = false;
      break;

    case 0x3: // both black
      setMotor(&motorLeft, direction, max_speed);
      setMotor(&motorRight, direction, max_speed);
      if (!on_black) {
        line_count += 1;
        on_black = true;
      }
//      if ( (line_count == 2) && (direction == 1)) {
//        line_count == 5;
////        mode = look_for_baton;
//        max_speed = 0x0D0;
////        setMotor(&motorLeft, 1, 0);
////        setMotor(&motorRight, 1, 0);
////        while(1);
////        lcd_putstring("DISTANCE");
//        startDistanceSensing();
////          lcd_command(CURSOR_HOME);
////          lcd_putstring("2 BLACK");
////          while (1);
//      } else if ( (line_count == 3) && (direction == 2)) {
//        pauseLineReading();
//        setMotor(&motorLeft, 0, 0);
//        setMotor(&motorRight, 0, 0);
//
//        setMotor(&motorClaw, 2, 0xAA0);
//        TIM2->CCR2 = 0xAA0;
//        delay2(3000000);
//        setMotor(&motorClaw, 0, 0x000);
//        TIM2->CCR2 = 0x0;

//        HardFault_Handler();

//      }
//      break;
    }

  }
}

void TIM3_IRQHandler(void) {
  TIM7->CR1 |= TIM_CR1_CEN;
  //ACK interrupt
  TIM3->SR &= ~(TIM_SR_UIF | TIM_SR_CC2IF);
  static uint16_t count = 1;
  if (count >= 9 && count < 90) {
//    pauseLineReading();
    TIM3->CCR2 = 0x4B0;
//    TIM3->CCER &= ~TIM_CCER_CC2E;
//    stopTimer(TIM17);
    // enable COMP1
    // for (int i = 0; i < 50000; i++);
    startTimer(TIM17);
    COMP->CSR |= COMP_CSR_COMP1EN;
  } else if (count >= 300) {
    count = 0;
    TIM3->CCR2 = 0x258;
    stopTimer(TIM17);
    resetTimer(TIM17);
  }
  count++;
}
