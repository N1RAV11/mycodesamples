#include "functions.h"

#define INDICATORLED GPIO_ODR_9

void initButton(void) {
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN;

  GPIOB->MODER &= ~(GPIO_MODER_MODER8 | GPIO_MODER_MODER9);
  GPIOB->MODER |= GPIO_MODER_MODER9_0;

  GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR8;
  GPIOB->PUPDR |= GPIO_PUPDR_PUPDR8_0;

  SYSCFG->EXTICR[2] |= SYSCFG_EXTICR3_EXTI8_PB;

  EXTI->FTSR |= EXTI_FTSR_TR8;
  EXTI->IMR |= EXTI_IMR_MR8;
}

void runMode(uint16_t config_values[], configMode *mode) {

  stopTimer(TIM6);
  resetTimer(TIM6);

  switch (*mode) {

  case startup:
    initTimer(TIM6, 1999, 47999);
    TIM6->DIER |= TIM_DIER_UIE;

    lcd_command(CLEAR);
    lcd_putstring("STARTUP");

    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;

    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    startTimer(TIM6);
    break;

  case idle:
    lcd_command(CLEAR);
    lcd_putstring("IDLE");
    GPIOB->ODR &= ~INDICATORLED;
    delay2(100000);
    GPIOB->ODR |= INDICATORLED;
    delay2(100000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(100000);
    GPIOB->ODR |= INDICATORLED;
    delay2(100000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(100000);
    GPIOB->ODR |= INDICATORLED;
    delay2(100000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(100000);

    saveValues(config_values);

    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    break;

  case start:
    lcd_command(CLEAR);
    lcd_putstring("START");
    GPIOB->ODR |= INDICATORLED;
    break;

  case line_f_wait:
    lcd_command(CLEAR);
    lcd_putstring("line_f_wait");

    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;

    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    EXTI->IMR |= EXTI_IMR_MR8;
//			setTimerParameters(TIM6, 1999, 47999);
    startTimer(TIM6);
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    break;

  case line_f_b:
    delay2(500000);
    ADC1->CHSELR = ADC_CHSELR_CHSEL5;
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    startTimer(TIM6);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    break;

  case line_f_w:
    GPIOB->ODR |= INDICATORLED;
    delay2(500000);
    ADC1->CHSELR = ADC_CHSELR_CHSEL5;
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    startTimer(TIM6);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    break;

  case line_b_wait:
    lcd_command(CLEAR);
    lcd_putstring("line_b_wait");

    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;

    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    EXTI->IMR |= EXTI_IMR_MR8;
//			setTimerParameters(TIM6, 1999, 47999);
    startTimer(TIM6);
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    break;

  case line_b_b:
    delay2(500000);
    ADC1->CHSELR = ADC_CHSELR_CHSEL5;
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    startTimer(TIM6);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    break;

  case line_b_w:
    GPIOB->ODR |= INDICATORLED;
    delay2(500000);
    ADC1->CHSELR = ADC_CHSELR_CHSEL5;
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    startTimer(TIM6);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    break;

  case colour_wait:
    lcd_command(CLEAR);
    lcd_putstring("colour_wait");

    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;
    delay2(250000);
    GPIOB->ODR |= INDICATORLED;
    delay2(250000);
    GPIOB->ODR &= ~INDICATORLED;

    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    EXTI->IMR |= EXTI_IMR_MR8;
    startTimer(TIM6);
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    break;

  case colour_green:
    delay2(500000);
    ADC1->CHSELR = ADC_CHSELR_CHSEL5;
    EXTI->IMR |= EXTI_IMR_MR8;
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    startTimer(TIM6);
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    break;

//		case colour_green:
//			GPIOB->ODR |= 0x01;
//			delay2(500000);
//			ADC1->CHSELR = ADC_CHSELR_CHSEL5;
//			ADC1->CR |= ADC_CR_ADSTART;
//			EXTI->IMR |= EXTI_IMR_MR0;
//			NVIC_EnableIRQ(EXTI4_15_IRQn);
//			startTimer(TIM6);
//			NVIC_EnableIRQ(TIM6_DAC_IRQn);
//			break;

  default:
    break;
  }
}

void changeMode(uint16_t config_values[], configMode * mode, uint8_t source) {
  switch (*mode) {
  case startup:
    if (source == TIMEOUT)
      *mode = idle;
    // always assuming that if not TIMEOUT, then PRESS
    else
      *mode = line_f_wait;
    break;

  case idle:
    if (source == PRESS) {
      *mode = start;
      calibration = 3;
    }
    break;

  case line_f_wait:
    if (source == TIMEOUT) {
      *mode = line_b_wait;
    } else {
      *mode = line_f_b;
      calibration = 1;
    }
    break;

  case line_f_b:
    *mode = line_f_w;
    break;

  case line_f_w:
    *mode = line_b_wait;
    calibration = 0;
    break;

  case line_b_wait:
    if (source == TIMEOUT) {
      *mode = colour_wait;
    } else {
      *mode = line_b_b;
      calibration = 1;
    }
    break;

  case line_b_b:
    *mode = line_b_w;
    break;

  case line_b_w:
    *mode = colour_wait;
    calibration = 0;
    break;

  case colour_wait:
    if (source == TIMEOUT) {
      *mode = idle;
    } else {
      *mode = colour_green;
      calibration = 1;
    }
    break;

//		case colour_red:
//			*mode = colour_green;
//			break;

  case colour_green:
    *mode = idle;
    calibration = 0;
    break;

  default:
    break;
  }

  if (calibration == 1)
    setTimerParameters(TIM6, 99, 47999);
  else if (calibration == 0)
    setTimerParameters(TIM6, 1999, 47999);
  else
    setTimerParameters(TIM6, 9, 4799);

  runMode(config_values, mode);
}

void readSensorValue(uint16_t *config_values, configMode mode) {
  uint16_t value;
  char string[17];

  ADC1->CR |= ADC_CR_ADSTART;
  while ((ADC1->ISR & ADC_ISR_EOC) == 0);
  ADC1->ISR &= ~ADC_ISR_EOC;
  value = ADC1->DR;

  switch (mode) {
    case line_f_b:
      config_values[0] = value;
      sprintf(string, "f_b: %u   ", config_values[0]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    case line_f_w:
      config_values[1] = value;
      sprintf(string, "f_w: %u   ", config_values[1]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    case line_b_b:
      config_values[2] = value;
      sprintf(string, "b_b: %u   ", config_values[2]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    case line_b_w:
      config_values[3] = value;
      sprintf(string, "b_w: %u   ", config_values[3]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    case colour_red:
      config_values[4] = value;
      sprintf(string, "c_r: %u   ", config_values[4]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    case colour_green:
      config_values[5] = value;
      sprintf(string, "c_g: %u   ", config_values[5]);
      lcd_command(CURSOR_HOME);
      lcd_putstring(string);
      break;

    default:
      break;
  }
}

void getMemoryValues(uint16_t *config_values) {
  for (int i = 0; i < 6; ++i) {
    config_values[i] = *(uint16_t*)(0x0800FC00 + (i * 2));
  }
}

void saveValues(uint16_t config_values[]) {
  // unlock sequence
  FLASH->KEYR = KEY1;
  FLASH->KEYR = KEY2;

  FLASH->CR |= FLASH_CR_PER;
  FLASH->AR = 0x0800FC00;
  FLASH->CR |= FLASH_CR_STRT;

  while ((FLASH->SR & FLASH_SR_EOP) == 0)
    ;
  FLASH->SR |= FLASH_SR_EOP;

  FLASH->CR &= ~FLASH_CR_PER;
  FLASH->CR |= FLASH_CR_PG;

  for (int i = 0; i < 6; ++i) {
    *(uint16_t*)(0x0800FC00 + (i * 2)) = config_values[i];
  }

  FLASH->CR |= FLASH_CR_LOCK;					// lock flash again
}

void waitForGreen(uint16_t config_values[]) {
  uint8_t positive;
  ADC1->CHSELR = ADC_CHSELR_CHSEL0;

  uint16_t trigger_level = config_values[5] * 0.8; // arbitrary number (80%)
  uint16_t reading = 0;
  while (1) {
    for (int i = 0; i < 3; ++i) {
      reading = readADCValue();
      if ((reading > trigger_level) && (reading < config_values[5])) positive++;
      delay2(10000);
    }

    if (positive == 3) break;
    else positive = 0;
  }
}

void setClockto48(void) {
  RCC->CR &= ~RCC_CR_PLLON;
  while ( (RCC->CR & RCC_CR_PLLRDY) != 0);
  RCC->CFGR |= RCC_CFGR_PLLMUL12;
  RCC->CR |= RCC_CR_PLLON;
  while ( (RCC->CR & RCC_CR_PLLRDY) == 0);

  RCC->CFGR |= RCC_CFGR_SW_PLL;
}

void robotSetup(uint16_t config_values[]) {
  setupMode = startup;
  initADC(TWELVE_BIT);
  getMemoryValues(config_values);
//  initPWM_B10_11();
  setupMotorTimer();
//  GPIOB->MODER |= 0x5555;
  init_LCD();
  initButton();

  runMode(config_values, &setupMode);
  while (setupMode != start);
}

void delay2(uint32_t microseconds) {
  /* Hangs for specified number of microseconds. */
  volatile uint32_t counter = 0;
  microseconds *= 3;
  for (; counter < microseconds; counter++) {
    __asm("nop");
    __asm("nop");
  }
}
