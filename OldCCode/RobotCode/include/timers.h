#ifndef TIMERS_H_
#define TIMERS_H_

#include "stm32f0xx.h"
#include "lcd_stm32f0.h"
#include <stdint.h>
#include <stdio.h>

#define KEY1 0x45670123
#define KEY2 0xCDEF89AB

void initTimer(TIM_TypeDef * timer, uint32_t ARR, uint32_t PSC);
void setTimerParameters(TIM_TypeDef * timer, uint32_t ARR, uint32_t PSC);
void startTimer(TIM_TypeDef * timer);
void stopTimer(TIM_TypeDef * timer);
void resetTimer(TIM_TypeDef * timer);

#endif /* TIMERS_H_ */
