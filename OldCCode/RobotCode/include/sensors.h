#ifndef SENSOR_H_
#define SENSOR_H_

#define KEY1 0x45670123
#define KEY2 0xCDEF89AB

#include "stm32f0xx.h"
#include "timers.h"
//#include "lcd_stm32f0.h"
//#include "functions.h"

//-----------------------------------------------------------------------------
//create a line sensor structure
typedef struct {
  uint16_t left; //left sensor value
  uint16_t right; //right sensor value
  uint16_t left_pin; //left sensor pin
  uint16_t right_pin; //right sensor pin
  GPIO_TypeDef *left_reg; //left sensor pin reg
  GPIO_TypeDef *right_reg; //right sensor pin reg
} LineSensor;
//-----------------------------------------------------------------------------

void initLineSensor(LineSensor * sensor);
void initBackLine(void);
void initColourSensor(void);
void initDistanceSensor(void);
void initBeamSensor(void);

void startLineReading(LineSensor * sensor);
void pauseLineReading(void);
void resumeLineReading(void);

void startBeamSensing(void);
void stopBeamSensing(void);

void startDistanceSensing(void);
void stopDistanceSensing(void);

#endif /* SENSOR_H_ */
