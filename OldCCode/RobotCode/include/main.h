#ifndef MAIN_H_
#define MAIN_H_

#include <stdint.h>
#include <stdio.h>

#include "stm32f0xx.h"
#include "lcd_stm32f0.h"
#include "functions.h"
#include "sensors.h"
#include "adc.h"
#include "motors.h"

bool setup = false;

char string[17];

#endif /* MAIN_H_ */
