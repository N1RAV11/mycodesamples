#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#define TIMEOUT 0x01
#define PRESS 0x02

#define true 1
#define false 0

#include "stm32f0xx.h"
#include "lcd_stm32f0.h"
#include "timers.h"
#include "adc.h"
#include "sensors.h"
#include <stdint.h>

uint8_t calibration;
typedef uint8_t bool;

typedef enum mode_e {startup, idle, start, line_f_wait, line_f_b, line_f_w,
					           line_b_wait, line_b_b, line_b_w,
					           colour_wait, colour_red, colour_green,
                     forward, look_for_baton, grab_baton, backwards, end} configMode;

configMode setupMode;

void setClockto48(void);
void robotSetup(uint16_t config_values[]);

void initButton(void);
void runMode(uint16_t config_values[], configMode *mode);
void changeMode(uint16_t config_values[], configMode * mode, uint8_t source);

void readSensorValue(uint16_t *config_values, configMode mode);
void getMemoryValues(uint16_t *config_values);
void saveValues(uint16_t config_values[]);
void waitForGreen(uint16_t config_values[]);

void delay2(uint32_t microseconds);

#endif /* FUNCTIONS_H_ */
