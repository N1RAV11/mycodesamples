#ifndef ADC_H_
#define ADC_H_

#include "stm32f0xx.h"

#define SIX_BIT 0x11
#define EIGHT_BIT 0x10
#define TEN_BIT 0x01
#define TWELVE_BIT 0x00


void initADC(uint8_t resolution);
void setADCResolution(uint8_t resolution);
uint16_t readADCValue(void);

#endif /* ADC_H_ */
