#ifndef MOTORS_H_
#define MOTORS_H_

#include "stm32f0xx.h"
#include "functions.h"

//-----------------------------------------------------------------------------
//create a motor structure
typedef struct {
  uint16_t fwd_pin; //fwd pin
  uint16_t rev_pin; //rev pin
  GPIO_TypeDef *fwd_reg; //fwd pin ODR
  GPIO_TypeDef *rev_reg; //rev pin ODR
  uint8_t state; // 0 = coast, 1 = fwd, 2 = rev, 3 = brake
  uint16_t set_speed;
  uint16_t current_speed;
  bool ramping;
  uint8_t ramp_speed;
  volatile uint32_t *tim_reg;
} Motor;
//-----------------------------------------------------------------------------
void setMotor(Motor *motor, uint8_t mode, uint16_t speed);
//-----------------------------------------------------------------------------
void setupMotorTimer(void);
void initMotor(Motor *motor);
void initPWM_B10_11(void);

#endif /* MOTORS_H_ */
