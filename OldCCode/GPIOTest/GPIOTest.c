#include <stdint.h>
#define STM32F051
#include "stm32f0xx.h"

// these create a 1 second delay using the Delay() function
#define DELAY1 1000
#define DELAY2 630

// constants for the push-button's IDR bit position to check for a button
#define SW0 0x01
#define SW1 0x02
#define SW2 0x04

void InitPorts(void); // used to initialise the LEDs and buttons on the board
void Delay(void); // creates a time delay
char CountUp(char value); // displays character to the LEDs and increments it
char CountDown(char value); // displays character to the LEDs and decrements it

char direction = 0; // the counter direction. 0: up, 1: down
char counter = 0; // the 8-bit counter value to display on the LEDs

int main(void) {
  InitPorts();

/* This was for part (f) of the practical. It's not required for the final code
  while (1) {
    if (!(GPIOA->IDR & SW0)) { // check if SW0 is pressed (logic low)
      GPIOB->ODR |= 0x01; // enable LED D0
    } else {
      GPIOB->ODR &= ~0x01; // disable LED D0
    }
  }
*/
  while (1) { // endless loop
    while (GPIOA->IDR & SW0); // wait until SWO is pressed (while not pressed)
    while (1) {
      if (!(GPIOA->IDR & SW1)) direction = 0; // if SW1 pressed, dir = up
      else if (!(GPIOA->IDR & SW2)) direction = 1; // if SW2 pressed, dir = down

      if (direction == 0) { // if the count direction is up
        counter = CountUp(counter); // display and increment the count value
      } else {
        counter = CountDown(counter); // display and decrement the count value
      }
      Delay(); // delay for 1 second
    }
  }
}

void InitPorts(void) {
  // enable GPIOA & B clock
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

  // LEDs
  // set LED pins to output mode
  GPIOB->MODER =
  GPIO_MODER_MODER0_0 | GPIO_MODER_MODER1_0 | GPIO_MODER_MODER2_0 |
  GPIO_MODER_MODER3_0 | GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 |
  GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0;

  // Buttons
  // set button pins to input mode (for robustness)
  GPIOA->MODER &=
  ~(GPIO_MODER_MODER0 | GPIO_MODER_MODER1 | GPIO_MODER_MODER2 |
    GPIO_MODER_MODER3);

  // enable pull up resistors for the 4 buttons
  GPIOA->PUPDR |=
  GPIO_PUPDR_PUPDR0_0 | GPIO_PUPDR_PUPDR1_0 | GPIO_PUPDR_PUPDR2_0 |
  GPIO_PUPDR_PUPDR3_0;
}

void Delay(void) {
  // this creates a time delay using nested for loops
  for (uint32_t i = 0; i < DELAY1; i++) {
    for (uint32_t j = 0; j < DELAY2; j++) {
      continue;
    }
  }
}

char CountUp(char value) {
  GPIOB->ODR = value; // display the "value" character on the LEDs
  return ++value;
}

char CountDown(char value) {
  GPIOB->ODR = value; // display the "value" character on the LEDs
  return --value;
}
