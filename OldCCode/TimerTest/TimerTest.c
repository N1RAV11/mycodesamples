//====================================================================
// INCLUDE FILES
//====================================================================
#include "lcd_stm32f0.h"
#include "stm32f0xx.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

//====================================================================
// GLOBAL CONSTANTS
//====================================================================

#define SW0 0x01
#define SW1 0x02
#define SW2 0x04
#define SW3 0x08

//====================================================================
// GLOBAL VARIABLES
//====================================================================

uint8_t minutes;
uint8_t seconds;
uint8_t hundredths;
char time[9];                                   // char array to hold the time string
uint8_t button = SW0;                           // indicates which button has been pressed last

//====================================================================
// FUNCTION DECLARATIONS
//====================================================================

// initializes GPIO Ports for switches & RG LED
void init_ports();

// initializes ADC for POT voltage measurement
void init_ADC();

// initializes NVIC to enable interrupts required
void init_NVIC();

// initializes EXTI to enable external interrupts for switches
void init_EXTI();

// initializes TIM14 for incrementing the stopwatch every 1 hundredth of a second
void init_TIM14();

void check_pb();

// function that reads POT0's voltage and returns it
uint8_t readPot0();

// function that determines the battery voltage (POT0) and updates the RG LED
void check_battery();

// function that converts a number into a string for displaying on the LCD
char * ConverttoBCD(float input, uint8_t wholeNums, uint8_t fracNums);

// a function that displays information on the LCD based on which switch is pressed
void display();

//====================================================================
// MAIN FUNCTION
//====================================================================
void main (void) {
  // set up all required peripherals
  init_ports();
  init_NVIC();
  init_TIM14();                                 // Initialize and begin TIM14
  init_LCD();

  lcd_putstring("Stop Watch");                  // Display string on line 1
  lcd_command(LINE_TWO);                        // Move cursor to line 2
  lcd_putstring("Press SW0...");                // Display string on line 2
  GPIOB->ODR = SW3;
  while (GPIOA->IDR & SW0);                     // wait until SW0 is pressed
  lcd_command(CLEAR);                           // clear LCD display
  lcd_putstring("Time");                        // Display string on line 1
  lcd_command(LINE_TWO);                        // Move to LCD line 2
  lcd_putstring("00:00.00");                    // display starting stopwatch
  TIM14->CR1 |= TIM_CR1_CEN;                    // enable TIM14's counter
  while(1) check_pb();                          // check if a button has been pressed
}

void init_ports(){
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;            // GPIOA for switches
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;            // GPIOB for LEDs
  GPIOB->MODER |=                               // set PB[3:0] to output mode
      (GPIO_MODER_MODER0_0 | GPIO_MODER_MODER1_0 |
          GPIO_MODER_MODER2_0 |GPIO_MODER_MODER3_0);
  GPIOA->PUPDR |=                               // Set pull-up resistors for switches
      (GPIO_PUPDR_PUPDR0_0 | GPIO_PUPDR_PUPDR1_0 |
          GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0);
}

void init_TIM14() {
  RCC->APB1ENR |= RCC_APB1ENR_TIM14EN;          // enable TIM14 clock
  TIM14->DIER |= TIM_DIER_UIE;                  // enable TIM14 interrupt
  // 0.01 second = (1/48MHz)(PSC + 1)(ARR + 1)
  TIM14->ARR = 599;
  TIM14->PSC = 799;
}

void init_NVIC() {
  NVIC_EnableIRQ(TIM14_IRQn);                   // enable TIM14 IQR
}

void display() {
  switch(button) {
  case SW0:
    sprintf(time, "%02u:%02u.%02u", minutes, seconds, hundredths); // formatting for timer display
    lcd_command(LINE_TWO);
    lcd_putstring(time);
    GPIOB->ODR = SW0;                           // display state on LEDS
    break;
  case SW1:
    GPIOB->ODR = SW1;                           // display state on LEDS
    break;
  case SW2:
    GPIOB->ODR = SW2;                           // display state on LEDS
    break;
  case SW3:
    lcd_command(LINE_TWO);
    lcd_putstring("00:00.00");                  // reset timer display
    GPIOB->ODR = SW3;                           // display state on LEDS
    break;
  }
}

void check_pb() {
  uint8_t button_states = GPIOA->IDR;
  // START
  if (((button_states & SW0) == 0)) {
    button = SW0;
    TIM14->CR1 |= TIM_CR1_CEN;                  // enable TIM14's counter
  }
  // LAP (only if state = START)
  else if (((button_states & SW1) == 0) && button == SW0) {
    button = SW1;
  }
  // STOP (only if state = START)
  else if (((button_states & SW2) == 0) && button == SW0) {
    button = SW2;
    TIM14->CR1 &= ~TIM_CR1_CEN;                 // disable TIM14's counter
    display();
  }
  // RESET
  else if (((button_states & SW3) == 0)) {
    TIM14->CR1 &= ~TIM_CR1_CEN;                 // disable TIM14's counter
    button = SW3;
    TIM14->CNT = 0;                             // clear TIM14's counter
    minutes = 0;
    seconds = 0;
    hundredths = 0;
    display();
  }
}

void TIM14_IRQHandler() {
  TIM14->SR &= ~TIM_SR_UIF;                     // acknowledge interrupt
  hundredths++;
  if (hundredths == 100) {
    hundredths = 0;
    seconds++;
  }
  if (seconds == 60) {
    seconds = 0;
    minutes++;
  }
  if (minutes == 60) {
    minutes = 0;
  }
  display();                                    // update timer display
}
