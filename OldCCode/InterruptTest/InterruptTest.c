//====================================================================
// INCLUDE FILES
//====================================================================
#include "lcd_stm32f0.h"
#include "stm32f0xx.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

//====================================================================
// GLOBAL CONSTANTS
//====================================================================

#define SW0 0x01
#define SW1 0x02
#define SW2 0x04
#define SW3 0x08

//====================================================================
// GLOBAL VARIABLES
//====================================================================

float battV;
uint32_t rainfall = 0;                          // actual rainfall = rainfall * 0.2mm

//====================================================================
// FUNCTION DECLARATIONS
//====================================================================

// initializes GPIO Ports for switches & RG LED
void init_ports();

// initializes ADC for POT voltage measurement
void init_ADC();

// initializes NVIC to enable interrupts required
void init_NVIC();

// initializes EXTI to enable external interrupts for switches
void init_EXTI();

// initializes TIM6 for check_battery() function to run every 1 second
void init_TIM6();

// function that reads POT0's voltage and returns it
uint8_t readPot0();

// function that determines the battery voltage (POT0) and updates the RG LED
void check_battery();

// function that converts a number into a string for displaying on the LCD
char * ConverttoBCD(float input, uint8_t wholeNums, uint8_t fracNums);

// a function that displays information on the LCD based on which switch is pressed
void display(uint8_t button);

//====================================================================
// MAIN FUNCTION
//====================================================================
void main (void) {
  // set up all required peripherals
  init_ports();
  init_ADC();
  init_NVIC();
  init_LCD();
  init_TIM6();

  lcd_putstring("EEE3017W Prac 6");             // Display string on line 1
  lcd_command(LINE_TWO);                        // Move cursor to line 2
  lcd_putstring("Nirav B & Adam R");            // Display string on line 2
  while (GPIOA->IDR & SW0);                     // wait until SW0 is pressed
  display(SW0);
  init_EXTI();                                  // enable push-button interrupts
  while(1);                                     // endless loop
}

void init_ports(){
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;            // GPIOA for switches
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;            // GPIOB for R/G led
  GPIOA->PUPDR |=                               // Set pull-up resistors for switches
      (GPIO_PUPDR_PUPDR0_0 | GPIO_PUPDR_PUPDR1_0 |
          GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR3_0);

  // set GPIO 10 & 11 to output mode
  GPIOB->MODER |= (GPIO_MODER_MODER10_0 | GPIO_MODER_MODER11_0);
  GPIOB->ODR |= GPIO_ODR_11;                    // switch on Green LED
}

void init_ADC(){
  RCC->APB2ENR |= RCC_APB2ENR_ADCEN;            // Enable ADC clock
  ADC1->CR |= ADC_CR_ADCAL;                     // start ADC calibration
  while (ADC1->CR & ADC_CR_ADCAL);              // wait for calibration to end
  ADC1->CR |= ADC_CR_ADEN;                      // Enable ADC
  ADC1->CFGR1 |= ADC_CFGR1_RES_1;               // Set 8 Bit Resolution
  ADC1->CHSELR |= ADC_CHSELR_CHSEL5;            // Enable channel 5
}

void init_NVIC() {
  NVIC_EnableIRQ(EXTI0_1_IRQn);                 // enable EXTI0 IQR
  NVIC_EnableIRQ(EXTI2_3_IRQn);                 // enable EXTI2 IQR
  NVIC_EnableIRQ(TIM6_DAC_IRQn);                // enable TIM6 IQR
}

void init_EXTI() {

  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN;     // enable SYSCFG clock

  // set falling edge trigger for all the switch interrupts
  EXTI->FTSR |=
      (EXTI_FTSR_TR0 | EXTI_FTSR_TR1 | EXTI_FTSR_TR2 | EXTI_FTSR_TR3);
  // unmask all the switch interrupts
  EXTI->IMR |=
      (EXTI_IMR_MR0 | EXTI_IMR_MR1 | EXTI_IMR_MR2 | EXTI_IMR_MR3);
}

void init_TIM6() {
  RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;           // enable TIM6 clock
  TIM6->DIER |= TIM_DIER_UIE;                   // enable TIM6 interrupt
  /* Eclipse seems to set the clock to 48MHz, so to generate a 1 sec delay,
   * 1 second = (1/48MHz)(PSC + 1)(ARR + 1)
   */
  TIM6->ARR = 9999;
  TIM6->PSC = 4799;
  TIM6->CR1 |= TIM_CR1_CEN;                     // enable TIM6's counter

}

uint8_t readPot0() {
  ADC1->CR |= ADC_CR_ADSTART;                   // start ADC conversion
  while ( !(ADC1->ISR & ADC_ISR_EOC) );         // wait for end of conversion
  return ADC1->DR;                              // return ADC data
}

void check_battery() {
  /* The next two lines takes POT0's value, and scales it to a value from
   * 0-24V. The constant that POT0's value is multiplied by will scale a
   * full POT0 voltage to slightly over 24, which allows us to then
   * limit that to 24, getting the full range 0-24V and not just below 24V
   */
  battV = readPot0() * 0.094118;                // scale POT value 0-24V
  if (battV > 24) battV = 24.00;

  if (battV < 14) {                             // low battery voltage
    GPIOB->ODR &= ~GPIO_ODR_11;                 // switch off green led
    GPIOB->ODR |= GPIO_ODR_10;                  // switch on red led
  } else {                                      // normal battery voltage
    GPIOB->ODR &= ~GPIO_ODR_10;                 // switch off red led
    GPIOB->ODR |= GPIO_ODR_11;                  // switch on green led
  }
}

char * ConverttoBCD(float input, uint8_t wholeNums, uint8_t fracNums) {

  static char string[17];                       // length of 1 LCD line + end char
  for (int i = 0; i < 17; i++) string[i] = ' '; // clear the string before using
  uint8_t length = wholeNums + fracNums;

  // shift number into whole numbers (and round to nearest whole number)
  uint32_t number = roundf(input * pow(10, fracNums));

  char digit;                                   // holds the current converted char

  for (int i = 0; i < length + 1; i++) {        // iterate through each digit of number
    if (i == fracNums) string[length-fracNums] = '.';// place a decimal separator
    else {
      digit = number % 10;                      // modulo division to get only the last digit
      string[length-i] = digit + 48;            // get ASCII conversion of number
      number /= 10;                             // remove the last digit from the number
    }
  }
  string[length + 1] = 0;                       // terminating character
  return string;
}

void display(uint8_t button) {
  lcd_command(CLEAR);                           // clear LCD display
  switch(button) {
  case SW0:
    lcd_putstring("Weather Station");           // Display string on line 1
    lcd_command(LINE_TWO);                      // Move cursor to line 2
    lcd_putstring("Press SW2 or SW3");          // Display string on line 2
    break;
  case SW1:
    lcd_putstring("Rain bucket tip");           // Display string on line 1
    break;
  case SW2:
    lcd_putstring("Rainfall:");                 // Display string on line 1
    lcd_command(LINE_TWO);                      // Move cursor to line 2
    // display rainfall amount on LCD with correct format and "mm" symbol afterwards
    lcd_putstring(ConverttoBCD(rainfall * 0.2, 4, 1));
    lcd_putstring(" mm");
    break;
  case SW3:
    lcd_putstring("Battery:");                  // Display string on line 1
    lcd_command(LINE_TWO);                      // Move cursor to line 2
    // display battery voltage on LCD with correct format and "V" symbol afterwards
    lcd_putstring(ConverttoBCD(battV, 2, 3));
    lcd_putstring(" V");
    break;
  }
}

void EXTI0_1_IRQHandler() {
  if ((EXTI->PR & EXTI_PR_PR0) == 1) {          // PA0 was pressed
    EXTI->PR |= EXTI_PR_PR0;                    // acknowledge interrupt
    display(SW0);
  }
  else if ((EXTI->PR & EXTI_PR_PR1) != 0) {     // PA1 was pressed
    EXTI->PR |= EXTI_PR_PR1;                    // acknowledge interrupt
    rainfall += 1;                              // increase rainfall (by 0.2mm)
    display(SW1);
  }
}

void EXTI2_3_IRQHandler() {
  if ((EXTI->PR & EXTI_PR_PR2) != 0) {          // PA2 was pressed
    EXTI->PR |= EXTI_PR_PR2;                    // acknowledge interrupt
    display(SW2);
  }
  else if ((EXTI->PR & EXTI_PR_PR3) != 0) {     // PA3 was pressed
    EXTI->PR |= EXTI_PR_PR3;                    // acknowledge interrupt
    display(SW3);
  }
}

void TIM6_DAC_IRQHandler() {
  TIM6->SR &= ~TIM_SR_UIF;                      // acknowledge interrupt
  check_battery();                              // monitor battery voltage
}
