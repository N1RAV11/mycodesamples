unsigned long startMillis;
unsigned long currentMillis;
const unsigned long timeoutPeriod = 1000 * 60 * 2;

int counter = 0;
int pedal = 0;

void setup() {
  pinMode(23, INPUT_PULLUP);//maybe set to 54-56?
  pinMode(24, INPUT_PULLUP);
  pinMode(25, INPUT_PULLUP);

  for (int i = 2; i <= 22; i++) //declaring all motor pins
  {
    pinMode(i, OUTPUT);
  }

  Serial.begin(9600);

}

void loop() {

  while (1) {
    //    currentMillis = millis();
    //    if (currentMillis - startMillis > timeoutPeriod) {
    //      startMillis = currentMillis;
    //      reset();
    //    }

    pedal = 0;

    if (digitalRead(23) > 0) {
      pedal = 1;
    }
    if (digitalRead(24) > 0) {
      pedal = 2;
    }
    if (digitalRead(25) > 0) {
      pedal = 3;
    }

    if (pedal > 0) {
      startMillis = currentMillis;
      break;
    }
  }

  
  if (counter == -1) {
    Serial.print(counter + 1);
    Serial.print(",");
    Serial.print("\n");
  }    else {
    Serial.print(counter + 1);
    Serial.print(",");
    Serial.print(pedal);
    Serial.print("\n");
    digitalWrite(((counter * 3) + pedal + 1), HIGH); //starts the chosen motor moving
  }

  counter++; //increment the counter

  //____________________________________________________________________________

  if (counter == 8) { //the loop that resets the motors
    delay(4000);
    counter = -1;


    for (int i = 1; i <= 21; i++)
    {
      digitalWrite(i, LOW);
    }
  }

  //____________________________________________________________________________

  delay(5000); //maybe reduce based on answering time

  while (1) { //the loop that waits for the pedals to be released

    if ((digitalRead(A0) + digitalRead(A1) + digitalRead(A2)) == 0) {
      break;
    }
    delay(20);
  }

}

void reset() {
  Serial.println(7);
  counter = 0;
}
