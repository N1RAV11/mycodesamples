import processing.serial.*;
import processing.video.*;
import java.io.BufferedWriter;
import java.io.FileWriter;


Serial myPort;                                    // Create object from Serial class

Movie CurrentMovie;
int count = 0;
String val;
int question;
int pedal;
String[] valuelist, arthursList;

String fileType = ".csv";
Boolean newMovie = true;
Boolean isIntro = true;

int lf = 10;                                      // ASCII linefeed 
String movieFile = "INTRO.mov";

void setup() {
  frameRate(25);
  //size(1280, 720);
  fullScreen();
  background(0);
  printArray(Serial.list());                     // print serial ports
  String port_name = Serial.list()[0];           // port communication //change the 0 to a 1 or 2 etc. to match your port
  myPort = new Serial(this, port_name, 9600);
  myPort.bufferUntil(lf);
  arthursList = new String[0];
}


void movieEvent(Movie m) {
  m.read();
}

void serialEvent(Serial myPort) {
  val = myPort.readString(); 
  myPort.clear();

  if (val != null) {
    valuelist = split(val, ',');

    if (valuelist.length==2) {
      print("Question: " + valuelist[0] + "\tAnswer: " + valuelist[1]);
    }
  } else {
    println("Input string was null");
    return;
  }
  
  if (CurrentMovie != null) {
    CurrentMovie.stop();
    CurrentMovie = null;
  }
  
  newMovie = true;
  isIntro = false;

  if (valuelist[0].equals("0")) {
    movieFile = "INTRO.mov";
    arthursList = new String[0];
    isIntro = true;

    println("Resetting cycle, clearing data output");
  }
  //q1
  else if (valuelist[0].equals("1")) {
    movieFile = "Q_1.mov";
    //append answer 1 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("2")) {
    movieFile = "Q_2.mov";
    //append answer 2 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("3")) {
    movieFile = "Q_3.mov";

    //append answer 3 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("4")) {
    movieFile = "Q_4.mov";

    //append answer 4 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("5")) {
    movieFile = "Q_5.mov";

    //append answer 5 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("6")) {
    movieFile = "Q_6.mov";

    //append answer 6 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("7")) {
    movieFile = "Q_7.mov";

    //append answer 6 to arthursList
    arthursList = append(arthursList, valuelist[1]);
  } else if (valuelist[0].equals("8")) {
    movieFile = "Outro.mov";

    //append answer 7 to arthursList and newline for new row in csv file.
    arthursList = append(arthursList, valuelist[1]+"\n");
    // turn list/array into one string seperated by comma.
    String arthursListString = join(arthursList, ",");
    //write arhturList to new row on file
    appendTextToFile(arthursListString);
  }
}

void draw() {
  if (newMovie) { 
    CurrentMovie = new Movie(this, movieFile);
    if (isIntro)
      CurrentMovie.loop();
    else
      CurrentMovie.play();

    newMovie = !newMovie;
  }

  if (CurrentMovie != null)
    image(CurrentMovie, 0, 0, width, height);
}

void keyPressed() {
  if (key == 'b' || key == 'B') {
    // turn list/array into one string seperated by comma.
    String arthursListString = join(arthursList, ",");
    //write arhturList to new row on file
    appendTextToFile(arthursListString);
  }
}
/**
 * Appends text to the end of a text file located in the data directory,
 * creates the file if it does not exist.
 * Can be used for big files with lots of rows,
 * existing lines will not be rewritten
 // Always ignore row 1!!!
 */
void appendTextToFile( String text) {

  // Generate date time stamp
  int d = day();    // Values from 1 - 31
  int m = month();  // Values from 1 - 12
  int y = year();   // 2003, 2004, 2005, etc.
  int h = hour();
  int min = minute();
  String s = String.valueOf(y) + String.valueOf(m) + String.valueOf(d) + "_" + String.valueOf(h) + String.valueOf(min);

  String filename = s + fileType;
  File f = new File(dataPath(filename));
  if (!f.exists()) {
    createFile(f);
  }
  try {
    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
    out.print(text);
    out.close();
  }
  catch (IOException e) {
    e.printStackTrace();
  }
}

/**
 * Creates a new file, including all subfolders
 */
void createFile(File f) {
  File parentDir = f.getParentFile();
  try {
    parentDir.mkdirs();
    f.createNewFile();
  }
  catch(Exception e) {
    e.printStackTrace();
  }
}   

String getTextAnswer(String number) {
  String output = "";
  switch(number) {
  case "1":
    output = "Y";
    break;
  case "2":
    output = "M";
    break;
  case "3":
    output = "N";
    break;
  }
  return output;
}
