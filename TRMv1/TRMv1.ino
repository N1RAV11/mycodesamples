//v3 New as of 08/08/2021
//with time on / time off

int latchPin = 3;
int clockPin = 2;
int dataPin = 5;
int clearPin = 4;
int enablePin = 6;

unsigned long delaytimer = 0;                       //the counter for the number of runs completed

byte dataArray[6] = {0};
long randNumber;

//array of block state, 0 is in, 1 is out
int stateArray[24] = {0};

void setup() {
  //set pins to output because they are addressed in the main loop
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(clearPin, OUTPUT);
  pinMode(enablePin, OUTPUT);

  digitalWrite(clearPin, 0);  //All registers cleared
  delay(50);
  digitalWrite(clearPin, 1);  //HIGH when no reset required
  digitalWrite(enablePin, 0); //LOW means enabled

  //read noise off Analog 0
  randomSeed(analogRead(0));

  Serial.begin(9600);

  //pull all blocks in
  /*              
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, B01010101); //assuming 01 is reverse
  digitalWrite(latchPin, HIGH);
  delay(500);
  for(int i=0; i<5; i++){
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, B00000000);
    digitalWrite(latchPin, HIGH);
    delay(500);
  }
  */
}

void loop() {

  randNumber = random(24);
  stateArray[randNumber] ^= 1; //XOR to switch states
  
  dataArray[randNumber/4] = (stateArray[randNumber]+1) << ((randNumber%4)*2);

  digitalWrite(latchPin, LOW);
  for(int c=0; c<6; c++){
    shiftOut(dataPin, clockPin, LSBFIRST, dataArray[c]);
    dataArray[c] = 0;
  }
  digitalWrite(latchPin, HIGH);

  delay(250); // enough time to toggle block 250

  digitalWrite(latchPin, LOW);
  digitalWrite(clearPin, 0);  //All registers cleared
  delay(10); //10
  digitalWrite(clearPin, 1);  //HIGH when no reset required
  digitalWrite(latchPin, HIGH);

  if((millis()-delaytimer)>60000){        //3min wait 360000
    Serial.print("delayStart");
    delaytimer=millis()+30000;            //add one 1 min 60000
    delay(30000);                         //delay for 1min 60000
    Serial.print("Done");
  }
}
