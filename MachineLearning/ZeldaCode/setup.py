from setuptools import setup, find_packages

setup(name='zelda-ml-course',
  version='0.1',
  packages=find_packages(),
  description='Field Prediction',
  author='Nirav Beni',
  author_email='niravbeni11@gmail.com',
  license='Zelda Learning Pty Ltd',
  install_requires=[
      'keras',
      'h5py'
  ],
  zip_safe=False)
