# Imported Functions and Libraries -----------------------------------------------------------------

import numpy as np
np.random.seed(42)
import tensorflow as tf
tf.set_random_seed(42)
from tensorflow.python.lib.io import file_io
from datetime import datetime
import time

import pandas as pd

import pickle
import argparse
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils

import keras
from keras.models import load_model, Sequential
from keras.layers import Dense, Dropout
from keras.layers import Activation

import keras.backend as K
import tensorflow as tf
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants
from tensorflow.python.saved_model.signature_def_utils_impl import predict_signature_def

tf.reset_default_graph()

# Variable Declarations ----------------------------------------------------------------------------

batch_size = 1          # was 5
num_classes = 6        # was 10 for old mock script (change when outputs change)
epochs = 100          # original  choice was 300 ---> 5000/10000 has better training accuracy, but 100 works fine for now (small dataset)
# Model Training -----------------------------------------------------------------------------------

def train_model(train_file, job_dir='./tmp/mlp', **args):
    logs_path = job_dir + '/logs/' + datetime.now().isoformat()
    print('-----------------------')
    print('Using train_file located at {}'.format(train_file))
    print('Using logs_path located at {}'.format(logs_path))
    print('-----------------------')

    dataframe = pd.read_csv(train_file, header=None)
    data = dataframe.values

    # x_train = data[:,0:10].astype(float)
    # x_train = x_train/50
    # y_train = data[:,10]                              old mock script data

    x_train = data[:,0:5].astype(float)
    #x_train = x_train/25                            # change if necessary (depending on how many questions)
    y_train = data[:,5]

    encoder = LabelEncoder()
    encoder.fit(y_train)
    encoded_Y = encoder.transform(y_train)
    y_train = np_utils.to_categorical(encoded_Y)

    model = Sequential()
    model.add(Dense(64, activation='relu', input_dim=5))            # input dim was 10 before
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(6, activation='sigmoid'))                       # dense layer was 10 before
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()

    history = model.fit(x_train, y_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        shuffle=True,
                        verbose=1
                        )

# Model Saving (Keras .h5 file) -------------------------------------------------------------------

    model.save('model.h5')

    with file_io.FileIO('model.h5', mode='r') as input_f:
        with file_io.FileIO(job_dir + '/model.h5', mode='w+') as output_f:
            output_f.write(input_f.read())

# Model Saving (Tensorflow .pb file for cloud prediction) ------------------------------------------

    K.set_learning_phase(0)
    config = model.get_config()
    weights = model.get_weights()
    new_model = Sequential.from_config(config)
    new_model.set_weights(weights)

    export_path = job_dir + "/export"
    builder = saved_model_builder.SavedModelBuilder(export_path)
    signature = predict_signature_def(
        inputs={'input': new_model.inputs[0]},
        outputs={'output': new_model.outputs[0]})

    with K.get_session() as sess:

        builder.add_meta_graph_and_variables(
            sess=sess,
            tags=[tag_constants.SERVING],
            clear_devices = True,
            signature_def_map={
                signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature}
        )
    builder.save()

# Command Line Arguments --------------------------------------------------------------------------

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Input Arguments
    parser.add_argument(
      '--train-file',
      help='GCS or local paths to training data',
      required=True
    )
    parser.add_argument(
      '--job-dir',
      help='GCS location to write checkpoints and export models',
      required=True
    )
    args = parser.parse_args()
    arguments = args.__dict__

    train_model(**arguments)

# -------------------------------------------------------------------------------------------------
