//*************************************************************************************
//*************************************************************************************
//Create Ball class
//Initialise x & y position
//Initialise radius r
//Initialise dx & dy movement increment 
//Create Ball constructor
//*************************************************************************************

class Ball{              
  float x = width/2;       
  float y = height/2;      
  float r = width/32;     
  float dy;               
  float dx;               
  
  Ball(){
  }
  
//*************************************************************************************
//show() Method:
//Display ball
//*************************************************************************************

  void show(){                                    
    panelLayer.fill(75);                            
    panelLayer.ellipse(x/scale,y/scale,r/2,r/2);   
  }
  
//*************************************************************************************
//edges() Method:
//Detect bounce region --> reverse direction
//Detect score region --> update score, reset ball  & paddle position
//*************************************************************************************

  void edges(){            
    if (y < 0 || y > height){   
      dy *= -1;           
    }
    if (x - r > width){    
      scoreL++;          
      reset(); 
      paddleL.reset();
      paddleR.reset();
    }
    if (x + r < -5){     
      scoreR++;           
      reset(); 
      paddleL.reset();
      paddleR.reset();
    }
  }
 
//*************************************************************************************
//update() Method:
//Update x & y position by dx & dy increment
//*************************************************************************************
 
  void update(){                  
    x = x + dx;                   
    y = y + dy;                    
  }

 //*************************************************************************************
 //checkLeft() Method:
 //Detect hit region --> calculate return angle
 //Move ball out of paddle
 //*************************************************************************************
  
  void checkLeft(Paddle p){                                                         
    if ((y - r < p.y + p.h/2) && (y + r > p.y - p.h/2) && (x - r < p.x + p.w/2)){    
      if (x > p.x){                                                                
        float dist = y - (p.y - p.h/2);                                              
        float angle = map(dist, 0, p.h, -radians(45), radians(45));                                
        dx = 5 * cos(angle);                                                       
        dy = 5 * sin(angle);                                                        
        x = p.x + p.w/2 + r;                                                       
      }
    }
  }
 
//*************************************************************************************
//checkRight() Method:
//Detect hit region --> calculate return angle
//Move ball out of paddle
//*************************************************************************************
  
  void checkRight(Paddle p){                                                         
    if ((y - r < p.y + p.h/2) && (y + r > p.y - p.h/2) && (x + r > p.x - p.w/2)){   
      if (x < p.x){                                                                  
        float dist = y - (p.y - p.h/2);                                            
        float angle = map(dist, 0, p.h, radians(225),  radians(135));                          
        dx = 5 * cos(angle);                                                       
        dy = 5 * sin(angle);                                                       
        x = p.x - p.w/2 - r;                                                         
      }
    }
  }

//*************************************************************************************
//reset() Method:
//Reset ball position
//Randomise ball trajectory (-45°,45°)
//*************************************************************************************
  
  void reset(){                        
    x = width/2;                       
    y = height/2;                       
    delay(1000);
    float angle = random(-PI/4, PI/4);   
    dx = 5 * cos(angle) ;              
    dy = 5 * sin(angle) ;               
    if (random(1) < 0.5){                
      dx *= -1;                    
    }
  }
} 
//*************************************************************************************
//*************************************************************************************
