//*************************************************************************************
//PONG*by*NIRAV*BENI*******************************************************************
//FOR*HIRSCH*&*MANN********************************************************************
//LIBRARIES*BY*ADAM*RAY*BRAUN**********************************************************
//*************************************************************************************

//*************************************************************************************
//Import serial libaries 
//*************************************************************************************

import com.hirschandmann.serial.matrix.*;
import processing.serial.*;
import com.hirschandmann.serial.d2xx.*;

//*************************************************************************************
//Set buffer variables
//*************************************************************************************

int window_w = 96;
int window_h = 96;
int scale = 4;
DigitalWallpaperPanel digitalWallpaper;
final int portNumber = 1;
PGraphics panelLayer;

//*************************************************************************************
//Initialise game variables
//*************************************************************************************

int scoreL = 0;                   
int scoreR = 0;                 
float tPos;                
int screenMode = 0;               
int xpos  = 0;                    
int xdir = 1;                  
int score;                       
int index = -1;              

//*************************************************************************************
//Initialise scoring array & string
//*************************************************************************************

int[] scores = new int[10];        
String[] names = new String[10];   
String typing = "";

//*************************************************************************************
//Initialise ball & paddle variables 
//*************************************************************************************

Ball ball;                         
Paddle paddleL;                    
Paddle paddleR;                    

//*************************************************************************************
//settings() Function:
//Set buffer settings
//*************************************************************************************

void settings() {
  size(window_w* scale, window_w * scale);
  noSmooth();
}

//*************************************************************************************
//setup() Function:
//Construct ball & paddle (left, right) instances of their objects
//Initaliase high score array 
//Initialise buffer
//*************************************************************************************

void setup() {
  ball = new Ball();               
  paddleL = new Paddle(true);     
  paddleR = new Paddle(false);   
  tPos = paddleL.w/2;              
  for (int i = 0; i < 10; i++){
      scores[i] = 0;
      names[i] = "N/A";
  }
  initBuffer();
}

//*************************************************************************************
//draw() Function:
//Start drawing
//Play screens
//Ending drawing
//*************************************************************************************

void draw() {
  bufferStart();
  screenSelection();
  bufferEnd(); 
}

//*************************************************************************************
//screenSelection() Function:
//Select display screen
//screenMode 0 is Home Screen
//ScreenMode 1 is Game Screen
//screenMode 2 is Score Screen
//screenMode 3 is Dummy Score Screen (Score Screen that takes no key inputs)
//*************************************************************************************

void screenSelection(){
if (screenMode == 0){
    homeScreen();
  } else if (screenMode == 1){
    gameScreen();
  } else if (screenMode == 2){
    scoreScreen();
  } else if (screenMode == 3){
    scoreScreen();
  }
}

//*************************************************************************************
//homeScreen() Function:
//Set text
//Display ball animation
//*************************************************************************************

void homeScreen(){                                                             
  homeText();
  homeAnimation();
}

//*************************************************************************************
//gameScreen() Function:
//Set text
//Update ball & paddles
//*************************************************************************************

void gameScreen(){
  gameText();
  updatePaddles();
  updateBall();
}

//*************************************************************************************
//scoreScreen() Function:
//Set text
//Display high score table (top 10 scores with names)
//*************************************************************************************

void scoreScreen(){
  scoreText();
  panelLayer.textSize(7);                                                                                                      
  for (int i = 0; i < 10; i++){                                                                                                   
    if (i < scores.length-1){                                                                                                    
      panelLayer.text(i+1 + ".       " +  scores[i] 
      + "      " + names[i], (width/scale)/2 - 24,(height/scale)/2 - 29 + 7*i);    
    } else {
      panelLayer.text(i+1 + ".     " +  scores[i]
      + "      " + names[i], (width/scale)/2 - 24,(height/scale)/2 - 29 + 7*i);       
    }
  }
}

//*************************************************************************************
//keyPressed() Function:
//Wait for keyboard inputs
//*************************************************************************************

void keyPressed(){   
  
//*************************************************************************************
//In Score Screen: 
//Take three character name
//Go to Dummy Score Screen (to bypass key input changes)
//*************************************************************************************

  if (screenMode == 2){
    typing += key;
    if (index != -1){
     names[index] = typing;
    }   //<>//
    if (typing.length()==3){
      typing = "";
      screenMode = 3;
    } 
  }
  
//*************************************************************************************
//If "→" pressed:
//Go to Game Screen
//*************************************************************************************

  if (key == CODED && (screenMode == 0 || screenMode ==2 || screenMode ==3)){    
    if (keyCode == RIGHT){
      delay(500);
      screenMode = 1;                    
      gameReset();
    }
  }  
  
//*************************************************************************************
//If "↓" pressed in Game Screen:
//Go to Score Screen
//Find score index 
//Sort table
//Write "ENTER" to index
//*************************************************************************************  
  
  if (key == CODED && screenMode == 1){     
    if (keyCode == DOWN){
      screenMode = 2;
      index = findIndex();
      if (index != -1){
        sortList(index);
        scores[index] = score;
        names[index] = "ENTER";
      }
    }
  }
  
//*************************************************************************************
//If "↓" pressed in HOME Screen:
//Go to Dummy Score Screen
//************************************************************************************* 
  
  if (key == CODED && screenMode ==0){     
    if (keyCode == DOWN){
      screenMode = 3;
    }
  }
  
//*************************************************************************************
//If "↑" pressed in Score or Dummy Score Screen:
//Go to HOME Screen
//************************************************************************************* 
  
  if (key == CODED && (screenMode == 2 || screenMode ==3)){      
    if (keyCode == UP){
      screenMode = 0;                             
    }
  }
  
//*************************************************************************************
//If "q" pressed in Game Screen:
//Move left paddle up
//If "a" pressed in Game Screen:
//Move left paddle down
//************************************************************************************* 
  
  if (key == 'q' && screenMode == 1){    
    paddleL.move(-5);                   
  } else if (key == 'a'){              
    paddleL.move(5);                     
  }   

//*************************************************************************************
//If "p" pressed in Game Screen:
//Move right paddle up
//If "l" pressed in Game Screen:
//Move right paddle down
//************************************************************************************* 

  if (key == 'p' && screenMode == 1){   
    paddleR.move(-5);                   
  } else if (key == 'l'){              
    paddleR.move(5);                    
  }
}

//*************************************************************************************
//keyReleased() Function:
//Reset paddle position on key release
//*************************************************************************************
  
void keyReleased(){                
  paddleReset();
 }

//*************************************************************************************
//initBuffer() Function:
//Initialise buffer
//*************************************************************************************

void initBuffer(){
  digitalWallpaper = new DigitalWallpaperPanel(this, portNumber);
  panelLayer = createGraphics(window_w,window_h);
  panelLayer.noSmooth();
  panelLayer.beginDraw();
  panelLayer.background(0);
  panelLayer.noStroke();
  panelLayer.endDraw();
}

//*************************************************************************************
//bufferStart() Function:
//Start buffer drawing
//*************************************************************************************

void bufferStart(){
  panelLayer.beginDraw();
  panelLayer.background(0);
  panelLayer.fill(75);
}

//*************************************************************************************
//bufferEnd() Function:
//End buffer drawing
//*************************************************************************************

void bufferEnd(){
  panelLayer.endDraw();
  PImage currentBufferContents = panelLayer.get();
  digitalWallpaper.setImage(currentBufferContents);
  image(panelLayer,0,0,width,height);
}

//*************************************************************************************
//homeAnimation() Function:
//Display ball animation
//*************************************************************************************

void homeAnimation(){
  panelLayer.ellipse(xpos, (height/scale)/2-5 ,5,5);                 
  xpos = xpos + xdir;                                                        
  if (xpos > (width/scale) - 2.5 || xpos < 1 ) {                               
    xdir *= -1;                                                        
  } 
}

//*************************************************************************************
//homeText() Function:
//Display Home Screen text
//*************************************************************************************

void homeText(){
  panelLayer.textSize(15);                                                    
  panelLayer.text("PONG", (width/scale)/2-20, (height/scale)/2-25);            
  panelLayer.textSize(8);                                                   
  panelLayer.text("PLAY →", (width/scale)/2-12, (height/scale)/2+22);       
  panelLayer.text("HIGH SCORES ↓", (width/scale)/2-29, (height/scale)/2+38);  
}

//*************************************************************************************
//gameText() Function:
//Display Game Screen text
//*************************************************************************************

void gameText(){
  panelLayer.textSize(10);                                                 
  panelLayer.text(scoreL, tPos, tPos*1.5);                                  
  panelLayer.text(scoreR, width/scale - 1.7*tPos, tPos*1.5);                
  panelLayer.textSize(8);                                                  
  panelLayer.fill(30);                                                    
  panelLayer.text("Q/A", tPos, height/scale - tPos);                       
  panelLayer.text("P/L", width/scale - 2.5*tPos, height/scale - tPos);     
  panelLayer.text("EXIT ↓", (width/scale)/2-10, tPos*1.5);                  
}

//*************************************************************************************
//scoreText() Function:
//Display Score Screen text 
//*************************************************************************************

void scoreText(){
  panelLayer.textSize(8);                                                                                                    
  panelLayer.text("HIGH SCORES", (width/scale)/2 - 27, (height/scale)/2-40);                                                    
  panelLayer.text("HOME ↑", (width/scale)/2-46, (height/scale)/2+45); 
  panelLayer.text("PLAY →", (width/scale)/2+18, (height/scale)/2+45);
}

//*************************************************************************************
//updatePaddles() Function:
//Display & update paddles
//*************************************************************************************

void updatePaddles(){
  paddleL.show();                  //call left Paddle show method
  paddleR.show();                  //call right Paddle show method
  paddleL.update();                //call left Paddle show method
  paddleR.update();                //call right Paddle show method
}

//*************************************************************************************
//updateBall() Function:
//Display & update ball
//Check for score, edges & collision
//*************************************************************************************

void updateBall(){
  ball.update();                   
  ball.edges();                    
  ball.checkLeft(paddleL);        
  ball.checkRight(paddleR);        
  ball.show();                     
}

//*************************************************************************************
//gameReset() Function:
//Reset ball & paddle position
//Reset scores
//*************************************************************************************

void gameReset(){
      ball.reset();                        //reset ball start 
      paddleL.reset();                     //reset left Paddle position
      paddleR.reset();                     //reset left Paddle position
      scoreL = 0;                          //reset left score
      scoreR = 0;                          //reset right score 
}

//*************************************************************************************
//paddleReset() Function:
//Stop paddles moving
//*************************************************************************************

void paddleReset(){
  paddleL.move(0);                 //reset dy to 0 to stop movement of left Paddle
  paddleR.move(0);                 //reset dy to 0 to stop movement of right Paddle
}

//*************************************************************************************
//findIndex() Function:
//Find top 10 entry index
//*************************************************************************************

int findIndex(){
  score = max(scoreL, scoreR);                   
  for (int i = 0; i < 10; i++){
    if (score > scores[i]){
      return i;
    }
  }
  return -1;
}

//*************************************************************************************
//sortList() Function:
//Shift scores down if new top 10 entry found
//*************************************************************************************

void sortList(int index){
  if (index != -1){
    for(int i = 9; i > index; i--){
      scores[i] = scores[i-1];
      names[i] = names[i-1];
     }
  }
}

//*************************************************************************************
//*************************************************************************************
